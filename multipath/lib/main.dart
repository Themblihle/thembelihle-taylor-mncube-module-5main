import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:multipath/screens/splash_screen.dart';
import 'package:multipath/screens/add_screen.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyA8xAwHJlxfzw8j_9uY1kEt_ksOy7LgT4E",
          authDomain: "multipathapp.firebaseapp.com",
          projectId: "multipathapp",
          storageBucket: "multipathapp.appspot.com",
          messagingSenderId: "927570092398",
          appId: "1:927570092398:web:42cdcee08519acf9d8617f"));
  runApp(const MainPage());
}

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Registration Page",
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: const SplashScreen(),
    );
  }
}

class Myhome extends StatefulWidget {
  const Myhome({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<Myhome> createState() => _MyhomeState();
}

class _MyhomeState extends State<Myhome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Form')),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const <Widget>[AddForm()],
      )),
    );
  }
}
