import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddForm extends StatefulWidget {
  const AddForm({Key? key}) : super(key: key);

  @override
  State<AddForm> createState() => _AddFormState();
}

class _AddFormState extends State<AddForm> {
  @override
  Widget build(BuildContext context) {
    TextEditingController jobController = TextEditingController();
    TextEditingController qualificationController = TextEditingController();
    TextEditingController locationController = TextEditingController();
    Future _addForms() {
      final employment = jobController.text;
      final qualification = qualificationController.text;
      final location = locationController.text;

      final ref = FirebaseFirestore.instance.collection("details").doc();
      return ref
          .set({
            "Occupation": employment,
            "Skills": qualification,
            "Location": location,
            "doc_id": ref.id
          })
          .then((value) => log("Details added!"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: TextField(
            controller: jobController,
            decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(15)),
                labelText: "Occupation"),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: TextField(
            controller: qualificationController,
            decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(15)),
                labelText: "Qualification"),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
          child: TextField(
            controller: locationController,
            decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                labelText: "Location"),
          ),
        ),
        ElevatedButton(
            onPressed: (() {
              _addForms();
            }),
            child: const Text("Add"))
      ],
    );
  }
}
